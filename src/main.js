import {
    createApp
} from 'vue'
import App from './App.vue'

createApp(App).mount('#app')

window.onload = function () {
    localStorage.getItem('dark-mode') ?
        document.querySelector('html').classList.add('dark-mode') : undefined
}
